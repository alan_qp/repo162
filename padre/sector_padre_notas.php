<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../micss/dashboard.css" rel="stylesheet">
  </head>
  <body>
    <?php
    include("../variables.php");
    include("clase_usuario_padre.php");
    session_start();//reanudamos una session si es que la hay
    if(!isset($_SESSION["padre"])){
      //redirigimos si no existe la session
      header("location:../index.php");
    }
    	require("nav_padre.php");
     ?>
     <div class="container">
       <div class="row">
         <div class="col-sm-3 col-md-2 sidebar">
           <ul class="nav nav-sidebar">
             <li class="active"><a href="sector_padre_comunicados.php">Mis Comunicados</a></li>
             <li ><a href="sector_padre_notas.php">Ver Notas</a></li>
           </ul>
           <ul class="nav nav-sidebar">
             <li><a href="#">Exportar</a></li>
             <li><a href="#">Reportes</a></li>
             <li><a href="sector_padre_archivos.php">Descargas</a></li>
           </ul>
         </div>
         <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

         </div>
       </div>
     </div>

     <script src="../js/jquery.js"></script>
 	 	<script src="../js/bootstrap.min.js"></script>
  </body>
</html>
