$(document).ready(function(){
  $("#password").focusin(function(){
    //$(this).addClass("fondoCaja");
  });
  $("#password").focusout(function(){
    if($(this).val().length>0 && $(this).val().length<6){
      $("#mensaje").html("<span style='color:#BA1C2E;'>Minimo 6 caracteres</span>");//span para poderle dar estilo al mensaje
    }
    else if($(this).val().length>=6 && $(this).val().length<=15){
      $("#mensaje").html("<span style='color:green;'>Contraseña valida</span>")
    }else{
      $("#mensaje").html("<span style='color:#BA1C2E;'>Error: maximo 15 caracteres</span>");
    }
  });

  $("#confirmacion").focusin(function(){

  });

  $("#confirmacion").focusout(function(){
    if($(this).val().length>0 && $(this).val().length<6){
      $("#mensaje").html("<span style='color:#BA1C2E;'>Minimo 6 caracteres</span>");
    }
    else if($(this).val().length>=6 && $(this).val().length<=15){
      if($("#password").val()===$("#confirmacion").val()){
        $("#mensajeConf").html("<span style='color:green;'>Tus contraseñas son iguales</span>");
      }else{
        $("#mensajeConf").html("<span style='color:#BA1C2E;'>Tus contraseñas no coinciden</span>");
      }
    }else {
      $("#mensajeConf").html("<span style='color:#BA1C2E;'>Error: maximo 15 caracteres</span>");
    }
  });

});
