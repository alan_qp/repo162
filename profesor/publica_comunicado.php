<?php
include("claseComunicado.php");
include("claseMateria.php");
include("clase_usuario_profesor.php");
include("../variables.php");
session_start();
if(!isset($_SESSION["profesor"])){
  header("location:../index.php");
}
$comunicado=$_POST["comunicado"];
$materia=$_POST["materi"];
$grado=$_POST["grado"];
$fecha="una fecha ficticia";
try{
  $base=new PDO("mysql:host=".Variables::$db_host. ";dbname=".Variables::$db_nombre, Variables::$db_usuario,Variables::$db_password);
  $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $base->exec(Variables::$juego_caracteres);

  //OBTENEMOS EL ID DE LA MATERIA A LA QUE PERTENECE EL COMUNICADO
  $sql="SELECT `ID_MATERIA` FROM `materia` WHERE `NOMBRE_MATERIA`=:nom and `ID_GRADO`=:grado";
  $resultado=$base->prepare($sql);
  $resultado->execute(array(":nom"=>$materia,":grado"=>$grado));
  $registro=$resultado->fetch(PDO::FETCH_ASSOC);
  $id_materia=$registro['ID_MATERIA'];

  //INSERTAMOS EL COMUNICADO
  $sql="INSERT INTO `comunicado`(`FECHA`, `MENSAJE`, `CI_PROFESOR`, `ID_MATERIA`) VALUES (:fech,:men,:cip,:idm)";
  $resultado=$base->prepare($sql);
  $resultado->execute(array(":fech"=>$fecha,":men"=>$comunicado,":cip"=>($_SESSION["profesor"]->ci_profesor),":idm"=>$id_materia));
  $resultado->closeCursor();
  header("location: sector_profesor_comunicados.php");
} catch (Exception $e) {
  if(!isset($id_materia))echo "Debes elegir una materia en la pestaña superior";
  else{
    echo "linea de error: " . $e->getLine()."</br>";
    die("Error: " . $e->getMessage());
  }
}
?>
