<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="../micss/dashboard.css" rel="stylesheet">
    <link rel="stylesheet" href="../micss/estilo_principal.css">
        <link rel="stylesheet" href="../micss/estilo_principal.css">
  </head>
  <body>

    <?php
    include("../variables.php");
    include("claseComunicado.php");
    include("claseMateria.php");
    include("clase_usuario_profesor.php");
    include("../clase_archivo.php");
    session_start();
    if(!isset($_SESSION["profesor"])){
      header("location:../index.php");
    }
    require("nav_profesor.php");
    ?>


     <div class="container">
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
              <ul class="nav nav-sidebar">
                <li><a href="sector_profesor_comunicados.php">Mis Comunicados</a></li>
                <li class="active"><a href="sube_archivos_profesor.php">Mis archivos</a></li>
                <li><a href="sector_notas_profesor.php">Subir Notas</a></li>
              </ul>
              <ul class="nav nav-sidebar">
                <li><a href="#">Exportar</a></li>
                <li><a href="#">Reportes</a></li>
                <li><a href="">Descargas</a></li>
              </ul>
            </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <center><h2 class="titulo">Bienvenido de su bandeja de archivos</h2></center>
          <!--formulario para subir archivos-->
        <div class="editaMensaje">
          <form action="sube_archivos_profesor.php" enctype="multipart/form-data" method="post"><br>
                <label>Subir Archivos:<br>
                  (el nombre no debe tener caracteres especiales)</label>
                <input type="file" name="archivo" required><br>
                <input type="text" name="comentario" placeholder="comentario" required>
             <button type="submit"  class="btn btn-primary">Subir Archivo</button><br><br>
          </form>
          <?php
          /*$nombre=$_FILES['archivo']['name'];
          echo "El nombre de tu archivo es " . $nombre . "<br>";

          $tipo=$_FILES['archivo']['type'];
          echo "El tipo de tu archivo es " . $tipo . "<br>";

          $tamanio=$_FILES['archivo']['size'];
          echo "El tamanio de tu archivo en bytes es: " . $tamanio . "<br>";*/

          $carpeta="../files/";
          opendir($carpeta);
          if(isset($_FILES['archivo']['tmp_name'])){
            if(is_uploaded_file($_FILES['archivo']['tmp_name'])){
              //ALMACENAMOS UN ARCHIVO EN EL DIRECTORIO FILES
              $path=$carpeta.$_FILES['archivo']['name'];
              copy($_FILES['archivo']['tmp_name'],$path);
              //ALMACENAMOS LOS DATOS DEL ARCHIVO EN LA BASE DE DATOS
              try{
                $base=new PDO("mysql:host=".Variables::$db_host. ";dbname=".Variables::$db_nombre, Variables::$db_usuario,Variables::$db_password);
                $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $base->exec(Variables::$juego_caracteres);
                $sql="INSERT INTO `archivo`( `NOMBRE_ARCHIVO`, `CI_PROFESOR`, `ID_MATERIA`, `COMENTARIO`)
                VALUES (:nomAr,:ci,:idm,:com)";
                $resultado=$base->prepare($sql);
                $resultado->execute(array(":nomAr"=>$_FILES['archivo']['name'],":ci"=>$_SESSION['profesor']->ci_profesor,":idm"=>8,":com"=>$_POST['comentario']));
                $numero_registro=$resultado->rowCount();
                if($numero_registro!=0)//si el usuario existe
                {
                    echo "Guardado correctamente en la BD";
                  }else{
                      echo "No se pudo guardar en la BD";
                  }
                  $resultado->closeCursor();
              } catch (Exception $e) {
                echo "linea de error: " . $e->getLine()."<br>";
                die("Error: " . $e->getMessage());
              }
            }
          }
           ?>
        </div>
          <table class="table table-striped table-bordered table-hover">
              <tr>
                <td><strong>Materia</strong></td>
                <td><strong>Profesor</strong></td>
                <td><strong>Comentario</strong></td>
                <td><strong>Nombre De Archivo</strong></td>
                <td><strong>Link de Descarga</strong></td>
              </tr>
            <tbody>
              <?php
                $directorio=opendir("../files/");
                $MisArchivos=$_SESSION["archivos"];
                while($archivo=readdir($directorio)){
                  if($archivo!='.' && $archivo !='..'){
                    for($i=0;$i<sizeof($MisArchivos);$i++){
                      if(($MisArchivos[$i]->nombre)==$archivo){
                        echo "<tr>
                        <td>
                          $MisArchivos[$i]->materia;
                        </td>
                        <td>
                          $MisArchivos[$i]->profesor;
                        </td>
                        <td>
                          $MisArchivos[$i]->comentario;
                        </td>
                          <td>
                            <a href='../descargas.php?archivo=$archivo'>$archivo</a>
                          </td>
                        </tr>";
                      }
                    }
                  }
                }
               ?>
            </tbody>
          </table>
      </div>
    </div>
  </div>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
  </body>
</html>
