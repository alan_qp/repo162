<?php
  session_start();
  if(!isset($_SESSION['usuario']))
    header("location: ../index.php");
  include("claseComunicado.php");
  include("claseMateria.php");
  include("clase_usuario_profesor.php");
  include("../clase_archivo.php");
  include("../variables.php");
  //carpeta donde se encuantran las fotos de usuario
  $path="../foto_perfil/";
  try{
  $base=new PDO("mysql:host=".Variables::$db_host. ";dbname=".Variables::$db_nombre, Variables::$db_usuario,Variables::$db_password);
  $base->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $base->exec(Variables::$juego_caracteres);
  $sql="SELECT * FROM PROFESOR WHERE `USUARIO`= :id";
  $resultado=$base->prepare($sql);
  $resultado->execute(array(":id"=>$_SESSION["usuario"]));
  $registro=$resultado->fetch(PDO::FETCH_ASSOC);
  //Datos del profesor logeado
  $profesor=new Profesor($registro['CI_PROFESOR'],$registro['NOMBRES'],$registro['APELLIDOS'],$registro['TELEFONO'],$registro['CORREO'],$path.$registro['FOTO_PATH'],$_SESSION['usuario'],$_SESSION['password']);
  unset($_SESSION['usuario']);
  unset($_SESSION['password']);
  $_SESSION['profesor']=$profesor;

   //Datos de las Materias que enseña
   $sql="SELECT m.NOMBRE_MATERIA,m.ID_GRADO FROM `profesor_materia` as pm,`materia`
   as m WHERE pm.`CI_PROFESOR`=:idprof AND m.`ID_MATERIA`=pm.`ID_MATERIA`";
   $resultado=$base->prepare($sql);
   $resultado->execute(array(":idprof"=>$_SESSION["profesor"]->ci_profesor));
   for($i=0;$registro=$resultado->fetch(PDO::FETCH_ASSOC);$i++)
     $MisMaterias[$i]=new Materia($registro['ID_GRADO'],$registro['NOMBRE_MATERIA']);
  $_SESSION["materias"]=$MisMaterias;

  //obtenemos los nombres de todos los archivos del profesor
  $sql="SELECT `NOMBRE_ARCHIVO`, `NOMBRE_MATERIA`,`NOMBRES`, `COMENTARIO` FROM `archivo` inner join `materia`
  on archivo.ID_MATERIA=materia.ID_MATERIA inner join  `profesor` on profesor.CI_PROFESOR=archivo.CI_PROFESOR
  WHERE archivo.CI_PROFESOR=:profe";

  $resultado=$base->prepare($sql);
  $resultado->execute(array(":profe"=>($_SESSION["profesor"]->ci_profesor)));

  for($i=0;$registro=$resultado->fetch(PDO::FETCH_ASSOC);$i++){
    $MisArchivos[$i]=new Archivo($registro['NOMBRE_ARCHIVO'],"../files",$registro['NOMBRE_MATERIA'],$registro['NOMBRES'],$registro['COMENTARIO']);
  }

   $_SESSION["archivos"]=$MisArchivos;
   $resultado->closeCursor();
   header("location: sector_profesor.php");
   } catch (Exception $e) {
     echo "linea de error: " . $e->getLine()."<br>";
     die("Error: " . $e->getMessage());
   }

?>
